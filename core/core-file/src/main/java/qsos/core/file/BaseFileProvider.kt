package qsos.core.file

import androidx.core.content.FileProvider

/**
 * @author : 华清松
 * 文件共享目录
 */
class BaseFileProvider : FileProvider()
